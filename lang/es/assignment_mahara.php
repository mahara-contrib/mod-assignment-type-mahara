<?
$string['pluginname'] = 'Portafolio Mahara';
$string['clicktopreview'] = 'pulsar para previsualizar a tamaño completo';
$string['clicktoselect'] = 'pulsar para seleccionar la página';
$string['nomaharahostsfound'] = 'No se han encontrado hosts Mahara.';
$string['noviewscreated'] = 'No ha creado ninguna página en {$a}.';
$string['noviewsfound'] = 'No se han encontrado páginas que concuerden en {$a}.';
$string['preview'] = 'Previsualizar';
$string['site'] = 'Sitio';
$string['site_help'] = 'Este ajuste le permite seleccionar desde que sitio Mahara los estudiantes enviarán sus páginas. (El sitio Mahara debe estar ya configurado para funcionar en red mnet con este sitio Moodle).';
$string['selectedview'] = 'Página enviada';
$string['selectmaharaview'] = 'Seleccione una de sus páginas de la lista mostrada del portafolio de {$a->name}, o <a href="{$a->jumpurl}">pulse aquí</a> para visitar {$a->name} y crear una página ahora mismo.';
$string['title'] = 'Título';
$string['typemahara'] = 'Portafolio Mahara';
$string['views'] = 'Páginas';
$string['viewsby'] = 'Páginas de {$a}';
